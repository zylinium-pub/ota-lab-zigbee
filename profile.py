#!/usr/bin/python

"""This profile allows the allocation of resources for over-the-air
operation in POWDERs indoor over-the-air (OTA) lab.

A map of the lab's layout is here:
FIXME: Need OTA layout link/image

The OTA Lab has a number of X310 SDRs connected to two broadband
antennas; one on the TX/RX port, and the other on RX2 port of channel 0.
Each X310 is paired with a compute node (by default a Dell d740).

The OTA Lab also has a number of B210s SDRs paired with an Intel NUC
small form factor compute node. These B210s are connected to broadband
antennas, one on each of the "channel A" TX/RX and RX2 ports.

This profile uses a disk image with Zigbee and UHD pre-installed

Resources needed to realize a basic zigbee setup consisting of ??

Instructions:

Killing/restarting the UE process will result in connectivity being interrupted/restored.

#### Troubleshooting

**No compatible RF-frontend found**

If srsenb fails with an error indicating "No compatible RF-frontend found",
you'll need to flash the appropriate firmware to the X310 and power-cycle it
using the portal UI. Run `uhd_usrp_probe` in a shell on the associated compute
node to get instructions for downloading and flashing the firmware. Use the
Action buttons in the List View tab of the UI to power cycle the appropriate
X310 after downloading and flashing the firmware. If srsue fails with a similar
error, try power-cycling the associated NUC.

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


class GLOBALS:
    ZIGBEE_IMG = "urn:publicid:IDN+emulab.net+image+FiveGExperiments//zy-zigbee"
    ULDEFLOFREQ = 3550.0
    ULDEFHIFREQ = 3570.0
    DLDEFLOFREQ = 3680.0
    DLDEFHIFREQ = 3700.0

def x310_node_pair(idx, x310_radio):
    radio_link = request.Link("radio-link-%d"%(idx))
    radio_link.bandwidth = 10*1000*1000

    node = request.RawPC("%s-comp"%(x310_radio.radio_name))
    node.hardware_type = params.x310_pair_nodetype
    node.disk_image = GLOBALS.ZIGBEE_IMG
    node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-radio"%(x310_radio.radio_name))
    radio.component_id = x310_radio.radio_name
    radio.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    radio_link.addNode(radio)

def b210_nuc_pair(idx, b210_node):
    b210_nuc_pair_node = request.RawPC("%s-b210" % b210_node.node_id)
    b210_nuc_pair_node.component_id = b210_node.node_id
    b210_nuc_pair_node.disk_image = GLOBALS.ZIGBEE_IMG
    b210_nuc_pair_node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    b210_nuc_pair_node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))

node_type = [
    ("d740",
     "Emulab, d740"),
    ("d430",
     "Emulab, d430")
]

portal.context.defineParameter("x310_pair_nodetype",
                               "Type of compute node paired with the X310 Radios",
                               portal.ParameterType.STRING,
                               node_type[0],
                               node_type)

lab_x310_names = [
    "ota-x310-1",
    "ota-x310-2",
    "ota-x310-3",
    "ota-x310-4",
]

portal.context.defineStructParameter("x310_radios", "OTA Lab X310 Radios", [],
                                     multiValue=True,
                                     itemDefaultValue=
                                     {},
                                     min=0, max=None,
                                     members=[
                                        portal.Parameter(
                                             "radio_name",
                                             "OTA Lab X310",
                                             portal.ParameterType.STRING,
                                             lab_x310_names[0],
                                             lab_x310_names)
                                     ])

ota_b210_names = [
    "ota-nuc1",
    "ota-nuc2",
    "ota-nuc3",
    "ota-nuc4",
]

portal.context.defineStructParameter("b210_nodes", "OTA Lab B210 Radios", [],
                                     multiValue=True,
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "node_id",
                                             "OTA Lab B210",
                                             portal.ParameterType.STRING,
                                             ota_b210_names[0],
                                             ota_b210_names)
                                     ],
                                    )

portal.context.defineParameter(
    "ul_freq_min",
    "Uplink Frequency Min",
    portal.ParameterType.BANDWIDTH,
    GLOBALS.ULDEFLOFREQ,
    longDescription="Values are rounded to the nearest kilohertz."
)
portal.context.defineParameter(
    "ul_freq_max",
    "Uplink Frequency Max",
    portal.ParameterType.BANDWIDTH,
    GLOBALS.ULDEFHIFREQ,
    longDescription="Values are rounded to the nearest kilohertz."
)
portal.context.defineParameter(
    "dl_freq_min",
    "Downlink Frequency Min",
    portal.ParameterType.BANDWIDTH,
    GLOBALS.DLDEFLOFREQ,
    longDescription="Values are rounded to the nearest kilohertz."
)
portal.context.defineParameter(
    "dl_freq_max",
    "Downlink Frequency Max",
    portal.ParameterType.BANDWIDTH,
    GLOBALS.DLDEFHIFREQ,
    longDescription="Values are rounded to the nearest kilohertz."
)

# Bind parameters
params = portal.context.bindParameters()

# Check frequency parameters.
#if params.ul_freq_min < 3550 or params.ul_freq_min > 3700 \
#   or params.ul_freq_max < 3550 or params.ul_freq_max > 3700:
#    perr = portal.ParameterError("CBRS uplink frequencies must be between 3550 and 3700 MHz", ['ul_freq_min', 'ul_freq_max'])
#    portal.context.reportError(perr)
#if params.ul_freq_max - params.ul_freq_min < 1:
#    perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ['ul_freq_min', 'ul_freq_max'])
#    portal.context.reportError(perr)
#if params.dl_freq_min < 3550 or params.dl_freq_min > 3700 \
#   or params.dl_freq_max < 3550 or params.dl_freq_max > 3700:
#    perr = portal.ParameterError("CBRS downlink frequencies must be between 3550 and 3700 MHz", ['dl_freq_min', 'dl_freq_max'])
#    portal.context.reportError(perr)
#if params.dl_freq_max - params.dl_freq_min < 1:
#    perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ['dl_freq_min', 'dl_freq_max'])
#    portal.context.reportError(perr)

# Now verify.
portal.context.verifyParameters()

# Lastly, request resources.
request = portal.context.makeRequestRSpec()

for i, x310_radio in enumerate(params.x310_radios):
    x310_node_pair(i, x310_radio)

for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i, b210_node)

# request.requestSpectrum(params.ul_freq_min, params.ul_freq_max, 0)
# request.requestSpectrum(params.dl_freq_min, params.dl_freq_max, 0)

portal.context.printRequestRSpec()
